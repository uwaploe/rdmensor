// Rdmensor samples the Mensor pressure sensor and publishes the data
// records to a Redis pub-sub channel
package main

import (
	"bytes"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"text/template"
	"time"

	mensor "bitbucket.org/uwaploe/go-mensor"
	"github.com/gomodule/redigo/redis"
	"github.com/tarm/serial"
)

var Version = "dev"
var BuildDate = "unknown"

var Usage = `Usage: rdmensor [options] serialdev interval

Read the pressure values from a Mensor pressure sensor attached to
SERIALDEV and publish the values to a Redis pub-sub channel. INTERVAL is
the sampling interval and is specified as sequence of decimal numbers and
a unit suffix, such as "5s" or "1m30s". At each interval, a burst of
samples is taken and averaged to create the output value.
`

// Default template for the output CSV records
const FORMAT = `{{.T|tfmt}},{{.D.pressure|printf "%.4f"}}`

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	dataFmt string        = FORMAT
	rdAddr  string        = "localhost:6379"
	rdChan  string        = "data.pr"
	inBaud  int           = 9600
	burst   time.Duration = 100 * time.Millisecond
	nSamps  int           = 10
	cmdFile string
)

type dataRecord struct {
	T time.Time
	D map[string]float32
}

// Template formatting function
func formatTime(t time.Time) string {
	return t.UTC().Format("2006-01-02 15:04:05.999")
}

func createMessage(t *template.Template, dr dataRecord) []byte {
	var buf bytes.Buffer
	t.Execute(&buf, dr)
	return buf.Bytes()
}

func publishMessage(conn redis.Conn, chName string, t *template.Template, dr dataRecord) {
	conn.Do("PUBLISH", chName, createMessage(t, dr))
}

func getSample(dev *mensor.Device, ticker *time.Ticker, count int) map[string]float32 {
	defer ticker.Stop()
	acc := newAccumulator()
	m := make(map[string]float32)
	for range ticker.C {
		x, err := dev.Pressure()
		if err != nil {
			log.Fatal(err)
		}
		m["pressure"] = float32(x)
		acc.addSample(m)
		count--
		if count == 0 {
			break
		}
	}
	return acc.getMean()
}

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&dataFmt, "fmt", dataFmt, "Output data format template")
	flag.StringVar(&rdAddr, "rdaddr", rdAddr, "Redis server HOST:PORT")
	flag.StringVar(&rdChan, "rdchan", rdChan, "Redis pub-sub channel")
	flag.IntVar(&inBaud, "baud", inBaud, "Sensor baud rate")
	flag.DurationVar(&burst, "burst", burst, "Sensor read interval")
	flag.IntVar(&nSamps, "n", nSamps, "Number of reads averaged for each sample")
	flag.StringVar(&cmdFile, "cmds", cmdFile, "Optional Mensor command file")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func main() {
	args := parseCmdLine()

	if len(args) < 2 {
		flag.Usage()
		os.Exit(1)
	}

	// Drop log timestamps when running under Systemd
	if _, ok := os.LookupEnv("JOURNAL_STREAM"); ok {
		log.SetFlags(0)
	}

	fm := template.FuncMap{
		"tfmt": formatTime,
	}
	tmpl := template.Must(template.New("rec").Funcs(fm).Parse(dataFmt))

	cfg := &serial.Config{
		Name:        args[0],
		Baud:        inBaud,
		ReadTimeout: time.Second * 5,
	}
	port, err := serial.OpenPort(cfg)
	if err != nil {
		log.Fatalf("Serial port open failed: %v", err)
	}

	dev := mensor.New(port)
	if cmdFile != "" {
		f, err := os.Open(cmdFile)
		if err != nil {
			log.Fatalf("Cannot open %q: %v", cmdFile, err)
		}
		defer f.Close()
		err = dev.Upload(f)
		if err != nil {
			log.Fatalf("Cannot send commands: %v", err)
		}
	}

	interval, err := time.ParseDuration(args[1])
	if err != nil {
		log.Fatalf("Invalid interval, %q: %v", args[1], err)
	}

	var conn redis.Conn

	if rdChan != "" {
		conn, err = redis.Dial("tcp", rdAddr)
		if err != nil {
			log.Fatalf("Cannot connect to Redis: %v", err)
		}
		defer conn.Close()
	}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	log.Printf("DOT Buoy Mensor data reader (%s)", Version)

	var rec dataRecord
	ticker := time.NewTicker(interval)
	for {
		select {
		case t0 := <-ticker.C:
			rec.D = getSample(dev, time.NewTicker(burst), nSamps)
			rec.T = t0
			if rdChan != "" {
				publishMessage(conn, rdChan, tmpl, rec)
			} else {
				fmt.Printf("%s\n", createMessage(tmpl, rec))
			}
		case <-sigs:
			return
		}
	}
}
