module bitbucket.org/uwaploe/rdmensor

go 1.16

require (
	bitbucket.org/uwaploe/go-mensor v0.3.0
	github.com/gomodule/redigo v1.8.5
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	golang.org/x/sys v0.0.0-20190225065934-cc5685c2db12 // indirect
)
